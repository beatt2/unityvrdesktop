//
// Created by admin on 2/12/2020.
//

#ifndef SOURCE_DESKINFO_H
#define SOURCE_DESKINFO_H


#include <d3d11.h>
#include <dxgi1_2.h>

#include "IUnityInterface.h"
#include "IUnityGraphics.h"
#include "IUnityGraphicsD3D11.h"


class DeskInfo
{
public:

    IDXGIOutputDuplication* DeskDup = nullptr;
    ID3D11Texture2D* Texture = nullptr;
    bool IsPointerVisible = false;
    int PointerX = -1;
    int PointerY = -1;
    int Width = -1;
    int Height = -1;
    int NeedReInit = 0;
    DXGI_OUTDUPL_FRAME_INFO FrameInfo;


};
namespace
{
    IUnityInterfaces* Unity= nullptr;
    int needReInit = 0;
    DeskInfo Desks[100];
    int NDesks = 0;
}



#endif //SOURCE_DESKINFO_H

