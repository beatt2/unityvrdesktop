cmake_minimum_required(VERSION 3.15)
project(Source)

set(CMAKE_CXX_STANDARD 17)

set(UNITY_SOURCE  "C:/Program Files/Unity/Hub/Editor/2019.3.0f6/Editor/Data/PluginAPI/")
set(HEADER_FILES ${UNITY_SOURCE}IUnityInterface.h ${UNITY_SOURCE}IUnityGraphics.h ${UNITY_SOURCE}IUnityGraphicsD3D11.h)
add_library(Source SHARED  ${HEADER_FILES} DllExportApi.cpp DeskInfo.h)
target_include_directories(Source PRIVATE  ${UNITY_SOURCE})
