//
// Created by admin on 2/12/2020.
//

#include <d3d11.h>
#include <dxgi1_2.h>


#include "IUnityInterface.h"
#include "IUnityGraphics.h"
#include "IUnityGraphicsD3D11.h"
#include "DeskInfo.h"


#pragma comment(lib, "dxgi.lib")



extern "C"
{
	void DeskClean()
	{
		for (int i = 0; NDesks > i; ++i)
		{
			if (Desks[i].DeskDup != nullptr)
			{
				Desks[i].DeskDup->Release();
			}

		}
		NDesks = 0;
	}

	int DeskAdd()
	{
		int i = NDesks;
		Desks[i].DeskDup = nullptr;
		Desks[i].Texture = nullptr;
		Desks[i].IsPointerVisible = false;
		Desks[i].PointerX = -1;
		Desks[i].PointerY = -1;
		Desks[i].Width = -1;
		Desks[i].Height = -1;
		Desks[i].NeedReInit = 0;
		NDesks++;
		return i;
	}

	UNITY_INTERFACE_EXPORT void UNITY_INTERFACE_API Desktop_Intitialize()
	{
		DeskClean();
		needReInit = 0;
		IDXGIFactory1* factory;
		// im not sure if chose the correct factory.
		CreateDXGIFactory1(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&factory));

		IDXGIAdapter1* adapter;
		// Enumerates both adapters (video cards) with or without outputs.
		for (int i = 0; (factory->EnumAdapters1(i, &adapter) != DXGI_ERROR_NOT_FOUND); i++)
		{
			IDXGIOutput* output;
			for (int j = 0; (adapter->EnumOutputs(j, &output) != DXGI_ERROR_NOT_FOUND); j++)
			{
				//structure describes the dimension of the output and the surface that contains the desktop image.
				DXGI_OUTPUT_DESC outputDesc;
				output->GetDesc(&outputDesc);

				MONITORINFOEX monitorInfo;
				monitorInfo.cbSize = sizeof(MONITORINFOEX);
				GetMonitorInfo(outputDesc.Monitor, &monitorInfo);

				{
					const auto iDesk = DeskAdd();
					Desks[iDesk].Width = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
					Desks[iDesk].Height = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;

					auto device = Unity->Get<IUnityGraphicsD3D11>()->GetDevice();
					IDXGIOutput1* output1;
					output1 =	reinterpret_cast<IDXGIOutput1*>(output);
					output1->DuplicateOutput(device, &Desks[iDesk].DeskDup);
				}

				output->Release();

			}

			adapter->Release();
		}

		factory->Release();
	}

	UNITY_INTERFACE_EXPORT void UNITY_INTERFACE_API UnityPluginLoad(IUnityInterfaces* unityInterfaces)
	{
		Unity = unityInterfaces;
		needReInit = 1;
	}

	

	UNITY_INTERFACE_EXPORT void UNITY_INTERFACE_API UnityPluginUnload()
	{
		DeskClean();
	}

	void UNITY_INTERFACE_API OnRenderEvent(int eventId)
	{
		for (int iDesk = 0; iDesk< NDesks; iDesk++)
		{
			if(Desks[iDesk].DeskDup == nullptr || Desks[iDesk].Texture == nullptr)
			{
				needReInit++;
				return;
			}

			IDXGIResource* resource = nullptr;

			const UINT timeout = 0; // ms
			HRESULT resultAcquire = Desks[iDesk].DeskDup->AcquireNextFrame(timeout, &Desks[iDesk].FrameInfo, &resource);
			if(resultAcquire != S_OK)
			{
				needReInit++;
				return;
			}

		
			Desks[iDesk].IsPointerVisible = (Desks[iDesk].FrameInfo.PointerPosition.Visible == TRUE);
			Desks[iDesk].PointerX = Desks[iDesk].FrameInfo.PointerPosition.Position.x;
			Desks[iDesk].PointerY = Desks[iDesk].FrameInfo.PointerPosition.Position.y;

			ID3D11Texture2D* texture;
			HRESULT resultQuery = resource->QueryInterface(__uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&texture));
			resource->Release();

			if(resultQuery != S_OK)
			{
				needReInit++;
				return;
			}



			ID3D11DeviceContext* context;
			auto device = Unity->Get<IUnityGraphicsD3D11>()->GetDevice();
			device->GetImmediateContext(&context);
			context->CopyResource(Desks[iDesk].Texture, texture);

			Desks[iDesk].DeskDup->ReleaseFrame();
			
			
		}

		needReInit = 0;
	}

	UNITY_INTERFACE_EXPORT UnityRenderingEvent UNITY_INTERFACE_API DesktopCapturePlugin_GetRenderEventFunc()
	{
		return OnRenderEvent;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_GetNDesks()
	{
		return NDesks;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_GetWidth(int iDesk)
	{
		return Desks[iDesk].Width;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_GetHeight(int iDesk)
	{
		
		return Desks[iDesk].Height;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_GetNeedReInit()
	{
		return needReInit;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_IsPointerVisible(int iDesk)
	{
		return Desks[iDesk].IsPointerVisible;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_GetPointerX(int iDesk)
	{
		return Desks[iDesk].PointerX;
	}

	UNITY_INTERFACE_EXPORT int UNITY_INTERFACE_API DesktopCapturePlugin_GetPointerY(int iDesk)
	{
		return Desks[iDesk].PointerY;
	}

	UNITY_INTERFACE_EXPORT void UNITY_INTERFACE_API DesktopCapturePlugin_SetTexturePtr(int iDesk, void* texture)
	{
		Desks[iDesk].Texture = reinterpret_cast<ID3D11Texture2D*>(texture);
	}
	

	
	
	
}

