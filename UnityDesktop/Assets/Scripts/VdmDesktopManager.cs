﻿#define VDM_SteamVR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


/// <summary>
/// Singleton
/// </summary>
public class VdmDesktopManager : MonoBehaviour
{
    public static VdmDesktopManager Instance;
    public static bool ActionInThisFrame = false;

    [Tooltip("Keyboard key to show/drag/hide")]
    public KeyCode KeyboardShow = KeyCode.LeftControl;

    [Tooltip("Keyboard key to zoom")] public KeyCode KeyBoardZoom = KeyCode.LeftAlt;

    
    /// <summary>
    /// Vive Functionality
    /// </summary>
#if VDM_SteamVR

    public enum ViveButton
    {
        None = 0,
        Trigger = 33,
        Grip = 2,
        Menu = 1,
        Touchpad = 32,
    }

    [Tooltip("Vive button to show/drag/hide")]
    public ViveButton ViveShow = ViveButton.Grip;

    [Tooltip("Vive button to zoom")] public ViveButton ViveZoom = ViveButton.Menu;

    [Tooltip("Vive button as mouse click (Left)")]
    public ViveButton ViveLeftClick = ViveButton.Trigger;

    [Tooltip("Vive touchpad as mouse left and right click")]
    public bool ViveTouchPadForClick = true;
#endif
    [Tooltip("Distance of the screen if showed with keyboard/mouse. Change it at runtime with with 'Show' + Mouse Wheel")]
    public float KeyboardDistance = 1;

    [Tooltip("If EnableZoomWithMenu is true, it's the distance between camera and monitor in Zoom Mode")]
    public float KeyboardZoomDistance = .5f;
    
    [Tooltip("If EnableZoomWithMenu is true, it's the distance between camera and monitor in Zoom Mode")]
    public float ControllerZoomDistance = 0.1f;

    [Tooltip("Monitor Scale Factor")] public float ScreenScaleFactor = .00025f;

    [Tooltip("Monitor Color Space")] public bool LinearColorSpace = false;

    [Tooltip("Multimonitor - 0 for all, otherwise screen number 1..x")]
    public int MultiMonitorScreen = 0;

    [Tooltip("Distance offset between monitors if MultiMonitorScreen==0")]
    public Vector3 MultiMonitorPositionOffset = new Vector3(1,0,0);

    [Tooltip("Render Scale - Supersampling. GPU intensive if >1")]
    [Range(1f, 2f)]
    public float RenderScale = 1.0f;

    [Tooltip("Unity Bug hack. If there are active UI elements that stop the playmode/VR, autoclose it.")]
    public bool EnableHackUnityBug = true;

    private System.Diagnostics.Process process = null;

    [DllImport("user32.dll")]
    private static extern IntPtr GetActiveWindow();

    [DllImport("DesktopCapture")]
    private static extern void DesktopCapturePlugin_Initialize();

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_GetNDesks();

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_GetWidth(int iDesk);

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_GetHeight(int iDesk);

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_GetNeedReInit();

    [DllImport("DesktopCapture")]
    private static extern bool DesktopCapturePlugin_IsPointerVisible(int iDesk);

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_GetPointerX(int iDesk);

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_GetPointerY(int iDesk);

    [DllImport("DesktopCapture")]
    private static extern int DesktopCapturePlugin_SetTexturePtr(int iDesk, IntPtr ptr);

    [DllImport("DesktopCapture")]
    private static extern IntPtr DesktopCapturePlugin_GetRenderEventFunc();

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool SetCursorPos(int X, int Y);

    [DllImport("user32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetCursorPos(out POINT lpPoint);

    [DllImport("user32.dll", SetLastError = true)]
    static extern uint SendInput(uint nInputs, ref INPUT pInputs, int cbSize);
    
    [StructLayout(LayoutKind.Sequential)]
    struct INPUT
    {
        public SendInputEventType type;
        public MouseKeybdhardwareInputUnion mkhi;
    }
    
    [StructLayout(LayoutKind.Sequential)]
    struct POINT
    {
        public int X;
        public int Y;

        public POINT(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    struct MouseKeybdhardwareInputUnion
    {
        /// <summary>Indicates the physical position of fields within the unmanaged representation of a class or structure.</summary>
        [FieldOffset(0)] public MouseInputData mi;

        [FieldOffset(0)] public KEYBDINPUT ki;

        [FieldOffset(0)] public HARDWAREINPUT hi;

    }

    [StructLayout(LayoutKind.Sequential)]
    struct KEYBDINPUT
    {
        public ushort wVk;
        public ushort wScan;
        public uint dwFlags;
        public uint time;
        public IntPtr dwExtraInfo;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct HARDWAREINPUT
    {
        public int uMsg;
        public short wParamL;
        public short wParamH;
    }

    struct MouseInputData
    {
        public int dx;
        public int dy;
        public uint mouseData;
        public VdmDesktop.MouseEventFlags dwFlags;
        public uint time;
        public IntPtr dwExtraInfo;
    }
    
    [Flags]
    enum MouseEventFlags : uint
    {
        MOUSEEVENTF_MOVE = 0x0001,
        MOUSEEVENTF_LEFTDOWN = 0x0002,
        MOUSEEVENTF_LEFTUP = 0x0004,
        MOUSEEVENTF_RIGHTDOWN = 0x0008,
        MOUSEEVENTF_RIGHTUP = 0x0010,
        MOUSEEVENTF_MIDDLEDOWN = 0x0020,
        MOUSEEVENTF_MIDDLEUP = 0x0040,
        MOUSEEVENTF_XDOWN = 0x0080,
        MOUSEEVENTF_XUP = 0x0100,
        MOUSEEVENTF_WHEEL = 0x0800,
        MOUSEEVENTF_VIRTUALDESK = 0x4000,
        MOUSEEVENTF_ABSOLUTE = 0x8000
    }

    enum SendInputEventType : int
    {
        InputMouse,
        InputKeyboard,
        InputHardware
    }
    
    public enum SPIF
    {
        None = 0x00,
        /// <summary>Writes the new system-wide parameter setting to the user profile.</summary>
        SPIF_UPDATEINIFILE = 0x01,
        /// <summary>Broadcasts the WM_SETTINGCHANGE message after updating the user profile.</summary>
        SPIF_SENDCHANGE = 0x02,
        /// <summary>Same as SPIF_SENDCHANGE.</summary>
        SPIF_SENDWININICHANGE = 0x02
    }

    [DllImport("user32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool SystemParametersInfo(uint uiAction, uint uiParam, ref IntPtr pvParam, SPIF fWinIni);//T any type
    
    public const uint SPI_SETMOUSETRAILS = 0x005D;
    public const uint SPI_GETMOUSETRAILS = 0x005E;

    private static int needReinit = 0;
    
#if VDM_SteamVR
    // Don't worry!
    // If you have a compilation error about missing SteamVR_TrackedObject,
    // comment the first line of this file, the "#define VDM_SteamVR" line.
    //private List<SteamVR_TrackedObject> Controllers = new List<SteamVR_TrackedObject>();
#endif

    private List<VdmDesktop> Monitors = new List<VdmDesktop>();

    private bool forceMouseTrail = false;//otherwise cursor is not visisble

    private void Start()
    {
        Instance = this;
        ReInit();
        GameObject monitBase = transform.GetChild(0).gameObject;
        monitBase.SetActive(false);

        int nScreen = DesktopCapturePlugin_GetNDesks();
        int iScreenIndex = 0;
        for (int i = 0; i < nScreen; i++)
        {
            GameObject monitor = GameObject.Instantiate(monitBase);

            if ((MultiMonitorScreen != 0) && (MultiMonitorScreen != (i + 1)))
            {
                continue;
            }

            monitor.name = "Monitor " + (i + 1);
            VdmDesktop desk = monitor.AddComponent<VdmDesktop>();
            desk.Screen
        }
    }
}