﻿#define VDM_SteamVR
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using UnityEngine;

// [RequireComponent(typeof(LineRenderer), typeof(Renderer), typeof(MeshCollider))]
public class VdmDesktop : MonoBehaviour
{
    private int screen;
    private int screenIndex;

    [DllImport("user32.dll")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

    [Flags]
    //Indicates that an enumeration can be treated as a bit field; that is, a set of flags.
    public enum MouseEventFlags
    {
        LEFTDOWN = 0x00000002,
        LEFTUP = 0x00000004,
        MIDDLEDOWN = 0x00000020,
        MIDDLEUP = 0x00000040,
        MOVE = 0x00000001,
        ABSOLUTE = 0x00008000,
        RIGHTDOWN = 0x00000008,
        RIGHTUP = 0x00000010
    }

    private VdmDesktopManager manager;
    private LineRenderer lineRenderer;
    private Renderer renderer;
    private MeshCollider collider;
    
    #if VDM_SteamVR
    private float lastLeftTriggerClick = 0;
    private float lastLeftTouchClick = 0;
    private float lastRightTouchClick = 0;
    private float distanceBeforeZoom = 0;
    private float lastShowClick = 0;
    private bool controllerAttach;
    #endif

    private bool zoom = false;
    private bool zoomWithFollowCursor = false;

    private Vector3 positionNormal;
    private Quaternion rotationNormal;
    private Vector3 positionZoomed;
    private Quaternion rotationZoomed;

    private float positionAnimationStart = 0;

    private float lastShowClickStart = 0;

    private void Start()
    {
        manager = transform.parent.GetComponent<VdmDesktopManager>();
        lineRenderer = GetComponent<LineRenderer>();
        renderer = GetComponent<Renderer>();
        collider = GetComponent<MeshCollider>();
        
        manager.con
    }


}
